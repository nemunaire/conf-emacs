(add-to-list 'c-style-alist
             '("epita"
	       (show-trailing-whitespace t)
	       (indent-tabs-mode . nil)
               (c-basic-offset . 2)
               (c-comment-only-line-offset . 0)
               (c-hanging-braces-alist     . ((substatement-open before after)))
               (c-offsets-alist . ((topmost-intro        . 0)
                                   (substatement         . +)
                                   (substatement-open    . 0)
                                   (case-label           . +)
                                   (access-label         . -)
                                   (inclass              . ++)
                                   (inline-open          . 0)))))

(c-add-style
 "e"
 '("gnu"
   (show-trailing-whitespace t)
   (indent-tabs-mode . nil)
   (tab-width . 8)
   (c-offsets-alist .
		    ((defun-block-intro . 3)
		     (statement-block-intro . 3)
		     (case-label . 1)
		     (statement-case-intro . 3)
		     (inclass . 3)
		     ))))

(c-add-style
 "novaquark"
 '("stroustrup"
   (show-trailing-whitespace t)
   (indent-tabs-mode . nil)
   (tab-width . 8)))
