;; E-mail adress
(custom-set-variables '(user-mail-address "nemunaire@nemunai.re")
                      '(query-user-mail-address nil))

;; Ispell check default language
(ispell-change-dictionary "francais" t)


(defvar cpu-number 8
  "Number of parallel processing units on this system")
(setq compile-command "make")

(setq c-default-style "epita")

;; Chargement de mes paramètres personnels
(if (file-exists-p "~/.private.el")
    (load-file "~/.private.el"))
