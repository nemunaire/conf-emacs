; Delete trailing whitespaces on save
(add-hook 'write-file-hooks 'delete-trailing-whitespace)

;; Mode to collapse code block
(add-hook 'c-mode-common-hook (lambda () (hs-minor-mode 1)))
(add-hook 'lisp-mode-hooks (lambda () (hs-minor-mode 1)))
(add-hook 'java-mode-hooks (lambda () (hs-minor-mode 1)))
(add-hook 'python-mode-hooks (lambda () (hs-minor-mode 1)))

; Auto insert C/C++ header guard
(add-hook 'find-file-hooks
	  (lambda ()
	    (when (and (memq major-mode '(c-mode c++-mode)) (equal (point-min) (point-max)) (string-match ".*\\.hh?" (buffer-file-name)))
	      (insert-header-guard)
	      (goto-line 3)
	      (insert "\n"))))
(add-hook 'find-file-hooks
	  (lambda ()
	    (when (and (memq major-mode '(c-mode c++-mode)) (equal (point-min) (point-max)) (string-match ".*\\.cc?" (buffer-file-name)))
	      (insert-header-inclusion))))

(add-hook 'sh-mode-hook
	  (lambda ()
            (insert-shebang-if-empty "/bin/sh")))

(add-hook 'ruby-mode-hook
	  (lambda ()
            (insert-shebang-if-empty "/usr/bin/ruby")))

; Golang
(add-hook 'go-mode-hook
	  (lambda ()
	    (set (make-local-variable 'compile-command) "go build")))

(add-hook 'go-mode-hook
	  (lambda ()
	    (when (buffer-empty-p)
	      (save-excursion
		(goto-char (point-min))
		(insert "package \n\nimport (\n\t\n)\n")))))

(add-hook 'before-save-hook 'gofmt-before-save)

(eval-after-load "go-mode"
  '(require 'flymake-go))
