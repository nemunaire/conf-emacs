(add-to-list 'auto-mode-alist '("/mutt" . mail-mode))

(add-hook 'mail-mode-hook          'turn-on-auto-fill)
(add-hook 'mail-mode-hook          'mail-abbrevs-setup)

;; colorizing multiply-quoted lines
(add-hook 'mail-mode-hook
          (lambda ()
            (font-lock-add-keywords nil
				    '(("^[ \t]*>[ \t]*>[ \t]*>.*$"
				       (0 'mail-multiply-quoted-text-face))
				      ("^[ \t]*>[ \t]*>.*$"
				       (0 'mail-double-quoted-text-face))))))
