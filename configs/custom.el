;; Remove useless features
(menu-bar-mode -1)
;(tool-bar-mode -1)
;(scroll-bar-mode -1)
(setq inhibit-startup-message t)

;;Save backup files into a specific directory, not in the working directry
(defun make-backup-file-name (file)
  (concat "~/.emacs.d/backup/" (file-name-nondirectory file) "~"))

;; Enable some minor mode changing mode line
(column-number-mode t) ; Show column number
(line-number-mode t)   ; Show line number
(display-time-mode t)  ; A clock in the mode line

;; Show matching parentheses
(show-paren-mode t)

;; Enable usefull modes
(global-font-lock-mode t)               ; syntax highlighting
(setq font-lock-maximum-decoration t)   ; max decoration for all modes
(auto-compression-mode t)               ; Auto decompress compressed files.
(setq set-mark-command-repeat-pop t)
(autoload 'nuke-trailing-whitespace "whitespace" nil t)
(setq delete-old-versions t)            ; delete oldversion file
(setq default-major-mode 'text-mode)    ; change default major mode to text
(global-auto-revert-mode t)             ; auto revert modified files
(dynamic-completion-mode)               ; dynamic completion

;; setting for auto-close brackets for electric-pair-mode regardless of current major mode syntax table
(electric-pair-mode t)
(setq electric-pair-pairs '(
                            (?\" . ?\")
                            (?\{ . ?\})
                            ) )

;; In linux, make copy/paste work with other apps
(setq x-select-enable-clipboard t)

;; Compilation window
(setq compilation-window-height 14)
(setq compilation-scroll-output t)

(require 'uniquify)
(setq uniquify-buffer-name-style 'post-forward-angle-brackets)

(setq-default ispell-program-name "aspell")

;; GDB use all existing window
(setq-default gdb-many-windows t)

;; Recognize test suite output
(require 'compile)
(add-to-list 'compilation-error-regexp-alist '("^\\(PASS\\|SKIP\\|XFAIL\\|TFAIL\\): \\(.*\\)$" 2 () () 0 2))
(add-to-list 'compilation-error-regexp-alist '("^\\(FAIL\\|XPASS\\): \\(.*\\)$" 2 () () 2 2))

;(require 'flymake)
;(add-hook 'find-file-hooks 'flymake-find-file-hook)
;(require 'flymake-cursor)

;;IswitchBuffer configuration
(require 'ido)
(ido-mode t)

;; Save and restore window layout
(defvar winconf-ring ())

(defun push-winconf ()
  (interactive)
  (window-configuration-to-register ?%)
  (push (get-register ?%) winconf-ring))

(defun pop-winconf ()
  (interactive)
  (set-register ?% (pop winconf-ring))
  (jump-to-register ?%))

(defun restore-winconf ()
  (interactive)
  (set-register ?% (car winconf-ring))
  (jump-to-register ?%))

;; highlight when > 80 cols
(defun eightycols nil
  (defface line-overflow
    '((t (:background "red" :foreground "black")))
    "Face to use for `hl-line-face'.")
  (highlight-regexp "^.\\{80,\\}$" 'line-overflow)
  )
;(add-hook 'find-file-hook 'eightycols)
