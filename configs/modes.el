;; Tiger mode
(autoload 'tiger-mode "tiger" "Load tiger-mode" t)
(add-to-list 'auto-mode-alist '("\\.ti[gh]$" . tiger-mode))

;; C++ mode
(add-to-list 'auto-mode-alist '("\\.l$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.y$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.ll$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.yy$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.xcc$" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.xhh$" . c++-mode))
(add-to-list 'auto-mode-alist '("Drakefile$" . c++-mode))

;; SH mode
(add-to-list 'auto-mode-alist '("\\.pro$" . sh-mode)) ; Qt .pro files
(add-to-list 'auto-mode-alist '("configure$" . sh-mode))
(add-to-list 'auto-mode-alist '("\\.bb$" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.bbclass$" . conf-mode))
(add-to-list 'auto-mode-alist '("\\.bbappend$" . conf-mode))

;; Go mode
(require 'go-mode-autoloads)

;; Changelog mode
(add-to-list 'auto-mode-alist '("COMMIT_EDITMSG" . change-log-mode))

;; Edje-mode
(require 'edje-mode)
(add-to-list 'auto-mode-alist '("\\.edc$" . edje-mode))

;; Org-mode
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
(setq org-startup-indented t)
(setq org-hide-leading-stars t)
(setq org-use-speed-commands t)
(setq org-ellipsis "\u2026" )
(setq org-completion-use-ido t)
(setq org-src-fontify-natively t) ;;syntax highlight code blocks

;; Markdown-mode
(add-to-list 'auto-mode-alist '("\\.md$" . markdown-mode))

;; Python-mode
;(when (load "flymake" t)
;  (defun flymake-pyflakes-init ()
;    (let* ((temp-file (flymake-init-create-temp-buffer-copy
;		       'flymake-create-temp-inplace))
;	   (local-file (file-relative-name
;			temp-file
;			(file-name-directory buffer-file-name))))
;      (list "pyflakes" (list local-file))))
;
;  (add-to-list 'flymake-allowed-file-name-masks
;	       '("\\.py\\'" flymake-pyflakes-init)))
;(add-hook 'find-file-hook 'flymake-find-file-hook)

;; mmm-mode
(add-hook 'mmm-mode-hook
	  (lambda ()
	    (set-face-background 'mmm-default-submode-face nil)))
