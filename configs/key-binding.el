;; Replace shell-command to permit executions through TRAMP
(global-set-key [(meta !)] 'eshell-command)

;; Goto line #
(global-set-key [(meta g)] 'goto-line)

;; Move between windnow
(global-set-key [C-left]   'windmove-left)
(global-set-key (kbd "M-O d")   'windmove-left)
(global-set-key (kbd "M-[ 1 ; 5 d")   'windmove-left)
(global-set-key [C-right]  'windmove-right)
(global-set-key (kbd "M-O c")   'windmove-right)
(global-set-key (kbd "M-[ 1 ; 5 c")   'windmove-right)
(global-set-key [C-up]     'windmove-up)
(global-set-key (kbd "M-O a")   'windmove-up)
(global-set-key [C-down]   'windmove-down)
(global-set-key (kbd "M-O b")   'windmove-down)

;; Compile file
(global-set-key (kbd "C-c M-c")  'compile)
(global-set-key (kbd "C-c c")    'recompile)
(global-set-key (kbd "C-c e")    'next-error)
(global-set-key (kbd "C-c g")    'gdb)

;; Get some stat on region
(global-set-key [(meta =)] 'stat-region)

;; Move in buffer
(global-set-key [C-home]   'beginning-of-buffer)
(global-set-key [C-end]    'end-of-buffer)
(global-set-key [home]     'beginning-of-line)
(global-set-key (kbd "M-O h") 'beginning-of-line)
(global-set-key [end]      'end-of-line)
(global-set-key (kbd "M-O f")      'end-of-line)
(global-set-key (kbd "M-[ 1 ; 3 c")      'right-word)
(global-set-key (kbd "M-[ 1 ; 3 d")      'left-word)

(global-set-key (kbd "C-c h") 'replace-string)
(global-set-key (kbd "C-c j") 'replace-regexp)
(global-set-key (kbd "C-c o") 'bury-buffer)
(global-set-key (kbd "C-c k") 'kill-this-buffer)
(put 'narrow-to-region  'disabled nil)

;; Magit
(global-set-key (kbd "C-x g") 'magit-status)
(global-set-key (kbd "C-x M-g") 'magit-dispatch-popup)

;; Don't shift-selection
(setq shift-select-mode nil)

;; BINDINGS :: C/C++

(require 'cc-mode)

(define-key
  c-mode-base-map
  [(control c) (w)]
  'c-switch-hh-cc)                                      ; switch between .hh and .cc
(define-key
  c-mode-base-map
  [(control c) (f)]
  'hs-hide-block)                                       ; fold code
(define-key
  c-mode-base-map
  [(control c) (s)]
  'hs-show-block)                                       ; unfold code
(define-key
  c-mode-base-map
  [(control c) (control n)]
  'c-insert-ns)                                         ; insert namespace
(define-key
  c-mode-base-map
  [(control c) (control s)]
  'c-insert-switch)                                     ; insert switch
(define-key
  c-mode-base-map
  [(control c) (control i)]
  'c-insert-if)                                         ; insert if
(define-key
  c-mode-base-map
  [(control c) (control b)]
  'c-insert-braces)                                     ; insert braces
(define-key
  c-mode-base-map
  [(control c) (control f)]
  'insert-fixme)                                      ; insert fixme
(define-key
  c-mode-base-map
  [(control c) (control d)]
  'c-insert-debug)                                      ; insert debug
(define-key
  c-mode-base-map
  [(control c) (control l)]
  'c-insert-class)                                      ; insert class
