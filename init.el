;;
;; emacs configuration
;;
;; Made by Némunaire <nemunaire@nemunai.re>

(defun may-load (path)
  "Load a file if it exists."
  (when (file-readable-p path)
    (load-file path)))

(defun reload ()
  "Reload configuration file"
  (interactive)
  (load-file "~/.emacs.d/init.elc"))

;; Compile init file when it is modified
(defun byte-compile-current-buffer ()
  "`byte-compile' current buffer if it's emacs-lisp-mode and compiled file exists."
  (interactive)
  (when (and (eq major-mode 'emacs-lisp-mode)
             (file-exists-p (byte-compile-dest-file buffer-file-name)))
    (byte-compile-file buffer-file-name)))
(add-hook 'after-save-hook 'byte-compile-current-buffer)

;; default emacs configuration directory
(defconst toc:emacs-config-dir "~/.emacs.d/configs/" "")

;; utility fonction to auto-load my package configurations
(defun toc:load-config-file (filelist)
  (dolist (file filelist)
    (message "Loading config file: %s..." file)
    (load (expand-file-name
	   (concat toc:emacs-config-dir file)))
    ))

(add-to-list 'load-path "~/.emacs.d/stuff/")

(require 'my-autoload)
(require 'my-c-mode)
(require 'my-elisp)
(require 'my-font)
(require 'my-layout)
(require 'my-lisp-mode)
(require 'my-python-mode)
(require 'vue-mode)

;; load my configuration files
(toc:load-config-file '("key-binding"
;;			"project"
			"editing"
			"coding-style"
			"tags"
			"hooks"
			"modes"
			"mail"
			"custom"
			"perso"
			))
